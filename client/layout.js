import {Template} from 'meteor/templating';

import './layout.html';
// import './nav.js';

import './views/map.js';
import './views/login.js';
import './views/footer.js';
import './views/mapSelect.js';
import './views/statcards.js';


Template.sidebar.onRendered( function() {
  $('.button-collapse').sideNav({
      menuWidth: 250, // Default is 240
      edge: 'left', // Choose the horizontal origin
      closeOnClick: false // Closes side-nav on <a> clicks, useful for Angular/Meteor
    }
  );
  
	setSelected();
});