console.log("--startup.js--");

//console.log("startup.js-subscribed to all ["+Offices.find().count() +"] offices");

setSelected = function() {
	var selected = $('#mapSelect option:selected').val();
	return Session.set('selected', selected);  
};

Template.body.helpers({
	'change select': function(e,tpl){
	    e.preventDefault();
	  var mapSelected = $('#mapSelect option:selected').val();        
	  console.log("body helper changed to " + mapSelected);
	  return Session.set('selected', mapSelected);  
	}
});