import { Meteor } from 'meteor/meteor';
import {Template} from 'meteor/templating';

import './forms.html';

Template.test.helpers({
  'currentDoc': function() {
    let id = Session.get('selectedCard');
    let doc = Offices.findOne({_id: id});
    // console.log(doc);
    return doc;
  }
});


AutoForm.addHooks('updateOfficeForm', {
    // Called when form does not have a `type` attribute or is 'normal'
    onSubmit: function (insertDoc, updateDoc, currentDoc) {
        // this.event.preventDefault();
        console.log('Just submitted form, from addform.js');
        //close modal on submit
        $('#updateModal').closeModal();

        this.done(); // submitted successfully, call onSuccess
        return false;
    },

  // Called when any submit operation succeeds
  onSuccess: function(formType, result) {
    console.log("Thanks for Submitting!");
    // console.log(this);
    $('#updateModal').closeModal();
  },
});