import {Template} from 'meteor/templating';

import './statcards.html';
import '../../lib/collections/declarations/offices.js';
import '../../lib/collections/schemas/offices.js';



console.log("--statcard.js--");


Template.registerHelper("objectToPairs",function(object){
let pairs = _.map(object, function(value, key) {
    return {
      key: key,
      value: value
    };
  });
return pairs;
});


Template.statcards.helpers({
  isDown: function() {
    //get value of this.stats, true if val == down
    let status = this.status;
    if (status === "down" || status === "closed" || status === "FALSE") return true;
  },
  selection: function() {
    //return Session.get('selected');
    return getSelected();
  },
  offices: function () {

    let status, state, arr;
    let filter = Session.get('selected');

    if (filter === "up") {
      console.log("GETTING "+filter);
      status = filter;
      arr = Offices.find( 
        { $and: [
          { $or: [{"stats.phone": status}, {"stats.phone": "TRUE"}] },
          { $or: [{"stats.network": status }, {"stats.network": "TRUE"}]}
        ]} 
      ).fetch();      
    }
    else if (filter==="down") {
      status = filter;
      console.log("only status "+status);
      arr = Offices.find( 
        { $or: [
          { "stats.phone": status },
          { "stats.phone": "FALSE" },
          { "stats.network": status },
          { "stats.network": "FALSE" }
        ]} 
      ).fetch();
    }
    else if (filter ==="closed") {
      state = filter;
      console.log("only state "+state);
      arr = Offices.find( 
        { $or: [
          { "stats.event": state },
          { "stats.event": "FALSE" }
        ]} 
      ).fetch();
    }
    else if (filter ==="all") {
      arr = Offices.find().fetch();
    }
    
    let labelArr = _.pluck(arr, "label");

    let statsObj = _.map(arr, function(o) {
      return o.stats;
    });
    let allNames = _.map(statsObj, function(p) {
      return _.keys(p);
    });
    let allProps = _.map(statsObj, function(p) {
      return _.keys(p);
    });    

    // console.log("SEARCHING FOR '"+filter+"' (phone/globe:"+ status+") (calendar:"+state+")");
    return arr;    

  },
  status: function() {

    // returns status for Colored StatusBar; if any status other than all-green, it's displayed.
    let status;
    let array = _.values(this.stats);
    //if office is down at all, return "warning".
    if (_.contains(array, "down") || _.contains(array, "FALSE") ) {
      status = "down";
    }
    //if office is up but closed, return "info"
    else if (_.contains(array, "closed") ) {
      status = "info";
    }
    else {status = "up";}
  
    return status;
  },
  props: function() {
    let object = this.stats;
    let symbolUp = "thumb_up";
    let symbolDn = "thumb_down";
    let symbolOpn = symbolUp;
    let symbolCls = "event_busy";
    let symbolInf = "info_outline";
    let symbolNet = "network_check";
    let symbolPhone = "phone";
    let symbolEvent = "event";

    let pairs = _.map(object, function(val, key) {
//change "up, down, closed" to font awesome icon class names.  <<status>> -> "fa-<<name>>"
      let serv, symbol;
      if (val === 'up' || val === 'open') symbol = symbolUp;
      else if (val === 'down' || val === 'FALSE') symbol = symbolDn;
      else if (val === 'closed' || val === 'FALSE' ) symbol = symbolCls;
        else {val = "info"; symbol = symbolInf;}
    
      if (key == "phone") serv = symbolPhone;
      else if(key == "network") serv = symbolNet;
      else if (key == 'event') serv = symbolEvent;

      return {
        service: serv,
        status: val,
        symbol: symbol
      };
    });
    let props = pairs;
    return props;
  },
  editing: function() {
    return Session.get('editing');
  },
});


Template.statcards.events({
  "click .modalTrigger": function(e , tpl) {
    e.preventDefault();
    $('#updateModal').openModal();
    // return Session.set('editing', true);
  },
  "submit form.form-edit": function(e, tpl) {
    let office;
    e.preventDefault();
    //office.info = tpl.$("input[name='editbox']").val();
    //need an update function to edit collection
    let info = e.target.value;
    console.log(info);

    if (info) {
      //Offices.insert(office);
      return Session.set('editing', false);
    }
  },
  "click .statcard": function(e, tpl) {
    let officeId = this._id;
    Session.set('selectedCard', officeId);
    $('#updateModal').openModal();
  }
});
