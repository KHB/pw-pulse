import {Template} from 'meteor/templating';

import './mapSelect.html';

/* 
Template.mapSelect.events({
     'change select': function(e,tpl){
   	    e.preventDefault();
			  var mapSelected = $('#mapSelect option:selected').val();        
        console.log("changed to " + mapSelected);
        return Session.set('selected', mapSelected);  
     }
 });
*/

Template.mapSelect.onRendered( function() {
  $(document).ready(function() {
    $('select').material_select();
  });
 	setSelected();
});

Template.mapSelect.events({
     'change select': function(e,tpl){
   	    //e.preventDefault();
				return setSelected();
     }
 });
