import {Template} from 'meteor/templating';

import './map.html';
import '../../lib/collections/declarations/offices.js';
import '../../lib/collections/schemas/offices.js';

console.log("--map.js--");

const Centers = {
  Eu : {"lat":19.9779264, "lng": -57.5665592}, 
  Nil : {"lat":32.275126, "lng":-33.977855}, 
  Na : {"lat":39.90973623453719, "lng":-105.29296875} 
};
  

const getColor = function(obj) {
    // returns overall status of object; if any status is down, the object is down.
    //return getStatus(obj) = down, info, or up
    let color;
    let array = _.values(obj.stats);

    let red = "rgba(255, 90, 46, 1)";
    let blue = "rgba(46, 188, 255, 1)";
    let yellow = "rgba(255, 255, 46, 1)";

    //if office is down at all, return "warning".
    if (_.contains(array, "down") || _.contains(array, "FALSE") ) {
      color = red;
    }
    //if office is up but closed, return "info"
    else if (_.contains(array, "closed")) {
      color = yellow;
    }
    else {color = blue;}
  
    return color;
};

Template.map.onCreated( function() {  

    this.subscribe('all_offices');
    GoogleMaps.load({
      v: '3',
      key: 'AIzaSyAtq9FTpaq8S7U9eZvxE_zH0tvSBlifbKQ'
    });

//COLLECTION DOES NOT EXIST OUT HERE YET ... FOR SOME REASON..

    GoogleMaps.ready('map', function(map) {
    let cursor = Offices.find();
    console.log("--map.js  GoogleMaps.ready() --");
    console.log("map.js-subscribed to all ["+ cursor.count() + "] offices");

   
    let officeArray = cursor.fetch();
   
    //Build array of only lat/longs (for each marker)
    let markers = [];
    let statii = [];
    let len = officeArray.length;
    for (let i = 0; i < len; i++) {
        let officeLoc = officeArray[i].location;
        let officeStat = officeArray[i].stats;
        statii.push(officeStat);
        markers.push(officeLoc);
    }
    // console.log("-->" + markers.length + " markers.");
    //--   list arrays
    //   console.log( JSON.stringify(statii[0]) );


    //--   Place Markers on map
    // Colors for markers
    
    // console.log(statii);

    let icon = {
      url: 'img/PWArrow_sm_blue.png',
      size: new google.maps.Size(18,18),
      origin: new google.maps.Point(0,0),
      anchor: new google.maps.Point(17,0)
    };
    
    for (let i = 0; i < len; i++) {
        let office = officeArray[i];
        let marker = new google.maps.Marker({
          position: office.location,
          map: map.instance,
          icon: icon,
          title: office.label,
        });

        //set icon depending on status
        // get value, assign a color to it, insert into img url as variable, plase that variable into image object.url:


        let cirColor = getColor(office);
       
        let circle = new google.maps.Circle({
            strokeColor: cirColor,
            strokeOpacity: 0.8,
            strokeWeight: 1,
            fillColor: cirColor,
            fillOpacity: 0.35,
            map: map.instance,
            center: office.location,
            radius: 50000,
        });


       
//============== INFOWINDOW ================

        // let infoContent= Blaze.toHTMLWithData(Template.infowindow);
        //console.log(infoContent);        
        // marker.info = new google.maps.InfoWindow({
        //   content: infoContent,
        //   maxWidth: 400
        // });

        // // Click for Status Alert
        // google.maps.event.addListener(marker, "click", function () {
        //   marker.info.setContent(this.info.content);
        //   marker.info.open(map.instance, this);
        //   Session.set('currentOffice', this.title);
        // });
        // Click to Zoom into region
        google.maps.event.addListener(marker,'click',function() {
          let currentZoom = map.instance.getZoom();
          if(currentZoom <= 4){
            map.instance.setZoom(17);
            map.instance.setCenter(this.getPosition());
          }
          else{
            map.instance.setZoom(2);
            map.instance.setCenter(this.getPosition());
          }
        });
        //Hover for Info-Windows
        // google.maps.event.addListener(marker, 'mouseover', function() {     
        //   marker.info.setContent(this.title);
        //   marker.info.open(map.instance, this);
        // });
        //Click to Zoom Out to default center
        google.maps.event.addDomListener(zoomOut, 'click', function() {
          map.instance.setZoom(2);
          map.instance.setCenter(Centers.Eu);
        });
    }
    
    //Echo lat/lng on click
    // google.maps.event.addDomListener(map.instance, 'click', function(e) {
    //   let point = [e.latLng.lat(), e.latLng.lng()];
          // console.log(point);
    // });

  });
});

Template.map.helpers({
  mapOptions: function() {
    if (GoogleMaps.loaded()) {
      return {
        center: new google.maps.LatLng(Centers.Na),
        zoom: 4,
        //mapTypeId:google.maps.MapTypeId.HYBRID,
        disableDefaultUI: true,
        scrollwheel: true,
        // Map styles; snippets from 'Snazzy Maps'.
        //BROKEN ORIGINAL
        // styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":30}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#000"},{"lightness":10}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.locational","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#fff"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":25}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":50}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]}]
        // LIGHT WITH BLUE WATER
        // styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
        // DARK WITH LIGHT BLUE WATER
        // styles: [{"featureType":"all","elementType":"geometry","stylers":[{"color":"#63b5e5"}]},{"featureType":"all","elementType":"labels.text.fill","stylers":[{"gamma":0.01},{"lightness":20}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"saturation":-31},{"lightness":-33},{"weight":2},{"gamma":0.8}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#000000"},{"lightness":"-100"},{"weight":"1"}]},{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"color":"#7da9ce"},{"weight":"2.54"},{"lightness":"0"},{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"lightness":30},{"saturation":30},{"color":"#505050"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#505050"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"saturation":20}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#000000"},{"lightness":"13"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"lightness":20},{"saturation":-20}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":10},{"saturation":-30}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#ededed"},{"lightness":"-57"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"lightness":"-56"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry.fill","stylers":[{"color":"#ededed"},{"lightness":"-56"}]},{"featureType":"transit.line","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#245d8c"}]}]
        // DARK WITH DARK BLUE WATER
        styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#0f252e"},{"lightness":17}]}]
       };
    }
  }
});

