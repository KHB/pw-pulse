import './declarations/offices.js';
import './schemas/offices.js';

console.log ("-= FIRST FILE: /lib/fixtures.js =-");
// FIRST FILE LOADED, ON BOTH CLIENT AND SERVER. {/lib/config/fixtures.js}


if (Meteor.isClient) {
	Meteor.subscribe('roles', {
		onReady: function() {
			// if (Roles.userHasRole(Meteor.userId(), "admin")) {
				// console.log("-= ADMIN: Logged In =-");

			Meteor.subscribe('all_offices', {
				onReady: function() {
					if (Offices.find().count() === 0) {
				  	console.log("-= OFFICES: Seeding from Client Insert Call =-");
						let json = require('./fixtures.json'); //with path
						_.each(json.Offices, function(doc) { 
						  
						  Offices.insert(doc , function(err, res){
						  	if (err) console.log(err);
							  	else console.log("Success!" + res);
						  });
						});
					} else {
						// console.log("-= Listings: Already Populated =-");
					}
				} 
			});

		} 
	});
} 	
