
Logs = new orion.collection('logs', {
  singularName: 'log', // The name of one of these items
  pluralName: 'logs', // The name of more than one of these items
  link: { title: 'Logs' },
  /**
   * Tabular settings for this collection
   */
  tabular: {
    // here we set which data columns we want to appear on the data table
    // in the CMS panel
    columns: [
      {
        data: "officeId",
        title: "Office"
      },
      { 
        data: "submitted", 
        title: "Created @" 
      },{ 
        data: "createdBy", 
        title: "Created By" 
      },{ 
        data: "comment", 
        title: "Comment" 
      }
    ]
  }
});

Logs.allow({
  insert: function(userId, doc) { return true;},
  update: function(userId, doc) { return true; },
  remove: function(userId, doc) { return false; },
});

export default Logs;