
Offices = new orion.collection('offices', {
  singularName: 'office', // The name of one of these items
  pluralName: 'offices', // The name of more than one of these items
  link: { title: 'Offices' },
  /**
   * Tabular settings for this collection
   */
  tabular: {
    // here we set which data columns we want to appear on the data table
    // in the CMS panel
    columns: [
      { 
        data: "name", 
        title: "Name" 
      },{ 
        data: "location", 
        title: "Location" 
      },{ 
        data: "stats", 
        title: "Statistics" 
      },
      {
        data: "note",
        title: "Note"
      },
      // {
      //   data: "logs",
      //   title: "Logs"
      // }
    ]
  }
});

Offices.allow({
  insert: function(userId, doc) { return true;},
  update: function(userId, doc) { return true; },
  remove: function(userId, doc) { return false; },
});

export default Offices;