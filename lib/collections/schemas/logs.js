import Logs from '../declarations/logs';
import Offices from '../declarations/offices';


const LogsSchema = new SimpleSchema({
  submitted: orion.attribute('createdAt'),
  createdBy: orion.attribute('createdBy', {
    label: 'User ID'
  }),
  comment: {
    type: String,
    optional: true
  }
});

const LogsSchema2 = new SimpleSchema({
  officeId: orion.attribute('hasOne', {
    label: 'Office',
    optional: false
  }, {
    collection: Offices,
    titleField: 'name',
    publicationName: 'randoString2'
  }),
  submitted: orion.attribute('createdAt'),
  createdBy: orion.attribute('createdBy', {
    optional: false,
    label: 'User ID'
  }),
  comment: {
    type: String,
    optional: true
  }
});

Logs.attachSchema(LogsSchema2);
