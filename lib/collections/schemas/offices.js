import Logs from '../declarations/logs';
import Offices from '../declarations/offices';

const OfficeSchema = new SimpleSchema({
  name: {
    type: String,
    unique: true
  },
  location: {
    type: Object
  },
  "location.lat": {
    type: Number,
    decimal: true
  },
  "location.lng": {
    type: Number,
    decimal: true
  },
  stats: {
    type: Object,
    label: "Statistics",
  },
  "stats.phone": {
    type: String,
    label: "Phone",
    autoValue: function() {
      if (this.isSet && this.value == "TRUE") return "up";
    },
    autoform: {
      afFieldInput: {
        type: 'boolean-radios',
        label: "Phones:",
        trueLabel: "Up",
        falseLabel: "Down"
      }
    }
  },
  "stats.network": {
    type: String,
    label: "Network",
    autoform: {
      afFieldInput: {
        type: 'boolean-radios',
        label: "Network: ",
        trueLabel: "Up",
        falseLabel: "Down"
      }
    }
  },
  "stats.event": {
    type: String,
    label: "Events",
    autoform: {
      afFieldInput: {
        type: 'boolean-radios',
        label: "Events: ",
        trueLabel: "Open",
        falseLabel: "Closed",
      }
    }
  },
  "stats.info": orion.attribute('hasMany', {
    // an array of log ids, 
    //hide htis and show 'logs.comment' field instead
    type: [String],
    label: "Office Logs",
    optional: true,
  }, {
    collection: Logs,
    titleField: 'comment',
    publicationName: 'randoString'
  }),
  note: {
    type: String,
    optional:true
  },
});

Offices.attachSchema(OfficeSchema);
