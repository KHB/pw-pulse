
import '../../lib/collections/declarations/offices.js';
import '../../lib/collections/schemas/offices.js';

console.log("--publications.js--");

Meteor.publish("all_offices", function () {
	let cursor = Offices.find();
	console.log("publishing all [" + cursor.count() + "] items");
	if (cursor) {
		return cursor;
	}
	return this.ready();
});

Meteor.publish("down_offices", function () {

		let cursor = Offices.find( 
			{ $or: [
				{ "stats.phone": "down" },
				{ "stats.network": "down" },
				{ "stats.event": "closed" }
			]} 
		);
	if (cursor) {
		console.log("publishing [" + cursor.count() + "] down offices");
		return cursor;
	}
	return this.ready();
});


Meteor.publish("up_offices", function () {
		let cursor = Offices.find( 
			{ $and: [
				{ "stats.phone": "up" },
				{ "stats.network": "up" },
				{ "stats.event": "open" }
			]} 
		);
	if (cursor) {
		console.log(cursor.count() + " up offices");
		return cursor;
	}
	return this.ready();
});
